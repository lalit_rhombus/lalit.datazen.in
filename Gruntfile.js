module.exports = function(grunt) {

   // Project configuration.
   grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // Tasks
    	// sngstore to make aworking sprite of the svg file
    	svgstore: {
		    options: {
		    	prefix : 'icon-', // This will prefix each ID
		    	cleanup: true,
		     	cleanupdefs: ['fill','style'],
		      	svg: { 
		      		style:"display:none;",
        			version: '1.1',
        			xmlns: 'http://www.w3.org/2000/svg'
		      	}
		    },
		    defaults:{
		    	files:{
		    		"public/asset/build/icon_defs.svg":["public/asset/svg/*.svg"]
		    	}
		    }
		},
		cssmin: {
				  my_target: {
				    files: {
				    }
				  }
				},
		sass: {                              // Task
		    dist: {                            // Target
		    	options: {                       // Target options
		        	style: 'compressed'
		      	},
		      	files: {                         // Dictionary of files
		        }
		    }
		},
		// added the autoprefixer for css run before the css min  
		autoprefixer: {
		  build: {
		    cwd: 'public/asset/build/',
		    src: [ '*.css'],
		    dest: 'public/asset/build/'
		  }
		},
    	// ugligy to min css and js
    	uglify: {
    		my_target: {
    			options: {
      				 beautify:false
      			},
      			files: {
      			}
    		}
  		},
		watch: {
			svgstore: {
			    files: 'public/asset/svg/*.svg',
			    tasks: [ 'svgstore' ]
			},
			sass:{
				files: ['public/asset/_include/partials/*.scss'],
			    tasks: [ 'sass' ]	
			},
			uglify:{
				files: 'public/asset/_include/script/*.js',
			    tasks: [ 'uglify' ]	
			}
		}
   });
    // Default task(s).
    grunt.registerTask('default', ['svgstore','uglify','sass','autoprefixer','cssmin']);
 	// Uglify to minify the css and js
 	grunt.loadNpmTasks('grunt-contrib-uglify');
 	// to just add auto prefix for browsers
 	grunt.loadNpmTasks('grunt-autoprefixer');
 	// to minify css
 	grunt.loadNpmTasks('grunt-contrib-cssmin');
 	// to make a working sprite of the svg file all together
 	grunt.loadNpmTasks('grunt-svgstore');
 	// add a watch for the pattern of file to do some tasks
 	grunt.loadNpmTasks('grunt-contrib-watch');
 	// add a sass preprocessor
 	grunt.loadNpmTasks('grunt-contrib-sass');
 };